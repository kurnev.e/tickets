# Web server for finding airline tickets

A web server powered by [rocket](https://rocket.rs/) framework that finds airline tickets with either no transfer or one transfer maximum. Results are sorted from lowest price to highest.

## Prerequisites

You should have [PostgreSQL](https://www.postgresql.org/) database `mydb` installed locally (or simply use [docker](https://hub.docker.com/_/postgres/)) or alternatively you can use an external one.  
To specify connection url either change `Rocket.toml` file or provide `ROCKET_DATABASES={mydb="%CONNECTION_URL%"}` env variable.

## Run

To run the web server execute following command:

```
cargo run
```

All diagnostic information and errors will be printed to stdout and stderr.

## Tests

Tests should be run sequentially and can be run with following command:

```
cargo test -- --test-threads 1
```

## Performance. Benchmarks

Simple benchmark tests were done with the help of [drill](https://github.com/fcsonline/drill) package. There are many pitfalls when writing benchmarks and I would analyze them very carefully, but they do give a basic overview of how program performs under `specific` load.  

To run benchmarks on your machine run following commands:

```
cargo install drill
drill --benchmark benchmark.yml --stats
```

Results for my machine `Intel(R) Core(TM) i5-8257U CPU @ 1.40GHz` with 20 concurrent requests for each endpoint:

```
Insert tickets            Median time per request   499ms
Insert tickets            Average time per request  962ms
Insert tickets            Sample standard deviation 1233ms

Search tickets            Median time per request   122ms
Search tickets            Average time per request  120ms
Search tickets            Sample standard deviation 21ms
```
