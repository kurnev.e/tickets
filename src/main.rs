use postgres::Client;
use rocket::fairing::AdHoc;
use rocket::{Build, Rocket};
use rocket_sync_db_pools::database;

mod search;
mod ticket;

#[macro_use]
extern crate rocket;

#[cfg(test)]
mod tests;

#[database("mydb")]
pub struct DbConnection(Client);

async fn init_db(rocket: Rocket<Build>) -> Rocket<Build> {
    DbConnection::get_one(&rocket)
        .await
        .expect("database mounted")
        .run(|conn| {
            conn.execute(
                "
                CREATE TABLE IF NOT EXISTS tickets (
                    id char(32) PRIMARY KEY,
                    departure_code char(3),
                    arrival_code char(3), 
                    departure_time integer,
                    arrival_time integer,
                    price integer
                );",
                &[],
            )
        })
        .await
        .expect("can init DB");

    rocket
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/api/v1", routes![ticket::batch_insert, search::search])
        .attach(DbConnection::fairing())
        .attach(AdHoc::on_ignite("Initializing database", init_db))
        .register("/api/v1/batch_insert", catchers![ticket::not_acceptable])
}

