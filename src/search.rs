use chrono::NaiveDate;
use postgres::types::Type;
use rocket::response::{self, Responder};
use rocket::serde::json;
use rocket::serde::{Deserialize, Serialize};
use rocket::{http::Status, serde::json::Json};
use rocket::{Request, Response};
use std::io::Cursor;

use crate::DbConnection;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct SearchData {
    pub departure_code: String,
    pub arrival_code: String,
    pub departure_date: String, // ISO 8601 DateTime<Utc>
    pub limit: i64,
}

#[derive(Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct SearchResult {
    pub solutions: Vec<SearchSolution>,
}

impl<'r> Responder<'r, 'static> for SearchResult {
    fn respond_to(self, _: &'r Request<'_>) -> response::Result<'static> {
        let body = json::serde_json::to_string(&self).unwrap();
        Response::build()
            .sized_body(body.len(), Cursor::new(body))
            .ok()
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct SearchSolution {
    pub ticket_ids: Vec<String>,
    pub price: i64,
}

#[post("/search", data = "<search_data>")]
pub async fn search(
    search_data: Json<SearchData>,
    conn: DbConnection,
) -> Result<SearchResult, Status> {
    let query_data = search_data.clone();
    let result_date = NaiveDate::parse_from_str(&query_data.departure_date, "%Y-%m-%d");

    let ticket_date;
    match result_date {
        Ok(date) => ticket_date = date,
        Err(err) => {
            eprintln!("{}", err);
            return Err(Status::InternalServerError);
        }
    }

    let statement_query = "
            SELECT
              concat(t1.id, ',', t2.id) as ids,
              sum(t1.price + t2.price) as price
            FROM tickets t1
            LEFT JOIN tickets t2 ON t1.arrival_code = t2.departure_code
            WHERE 1=1
               AND t1.departure_code = $1
               AND t2.arrival_code = $2
               AND t1.arrival_code != t1.departure_code
               AND t2.arrival_code != t2.departure_code
               AND date(to_timestamp(t1.departure_time)) = $3
               AND to_timestamp(t2.departure_time) - to_timestamp(t1.arrival_time) <= INTERVAL '24 hours'
               AND to_timestamp(t2.departure_time) - to_timestamp(t1.arrival_time) >= INTERVAL '3 hours'
            GROUP BY t1.id,t2.id
        UNION
            SELECT
              t3.id as ids,
              t3.price as price
            FROM tickets t3
            WHERE 1=1
               AND t3.departure_code = $1
               AND t3.arrival_code = $2
               AND t3.arrival_code != t3.departure_code
               AND date(to_timestamp(t3.departure_time)) = $3
            ORDER BY price
            LIMIT $4;";

    let statement_result = conn
        .run({
            move |conn| {
                conn.prepare_typed(
                    statement_query,
                    &[Type::VARCHAR, Type::VARCHAR, Type::DATE, Type::INT8],
                )
            }
        })
        .await;

    let statement;
    match statement_result {
        Ok(statement_value) => statement = statement_value,
        Err(err) => {
            eprintln!("{}", err);
            return Err(Status::InternalServerError);
        }
    }

    let select_result = conn
        .run(move |conn| {
            conn.query(
                &statement,
                &[
                    &query_data.departure_code,
                    &query_data.arrival_code,
                    &ticket_date,
                    &query_data.limit,
                ],
            )
        })
        .await;

    match select_result {
        Ok(rows) => {
            let mut solutions: Vec<SearchSolution> = vec![];
            for row in rows {
                let id: &str = row.get(0);
                let price: i64 = row.get(1);
                let ids = id.split(",").map(|s: &str| s.to_string()).collect();
                let solution = SearchSolution {
                    ticket_ids: ids,
                    price,
                };
                solutions.push(solution);
            }
            eprintln!("{:?}", solutions.len());
            Ok(SearchResult { solutions })
        }
        Err(err) => {
            eprintln!("{}", err);
            return Err(Status::InternalServerError);
        }
    }
}
