mod tests {
    use crate::rocket;
    use crate::search::{SearchData, SearchResult, SearchSolution};
    use crate::ticket::{StatusResponse, Ticket, Tickets};
    use rand::distributions::Alphanumeric;
    use rand::seq::SliceRandom;
    use rand::Rng;
    use rocket::http::Status;
    use rocket::local::blocking::Client;
    use rocket::serde::json;
    use rocket::serde::{Deserialize, Serialize};
    use std::iter;

    use postgres;

    #[derive(Serialize, Deserialize)]
    #[serde(crate = "rocket::serde")]
    struct FakeTicket {
        not_tickets: Vec<String>,
    }

    #[test]
    fn should_insert_1_or_10000_ticket() {
        clean_db();
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let tickets = generate_tickets_json_for_insert(1);
        let response = client.post("/api/v1/batch_insert").body(tickets).dispatch();
        assert_eq!(response.status(), Status::Ok);
        let status = StatusResponse {
            status: String::from("success"),
        };
        assert_eq!(
            response.into_string().unwrap(),
            json::serde_json::to_string(&status).unwrap()
        );

        let tickets: String = generate_tickets_json_for_insert(10000);
        let response = client.post("/api/v1/batch_insert").body(tickets).dispatch();
        assert_eq!(response.status(), Status::Ok);
        assert_eq!(
            response.into_string().unwrap(),
            json::serde_json::to_string(&status).unwrap()
        );
        clean_db();
    }

    #[test]
    fn should_not_insert_0_nor_10001_tickets() {
        clean_db();
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let tickets = generate_tickets_json_for_insert(0);
        let response = client.post("/api/v1/batch_insert").body(tickets).dispatch();
        assert_eq!(response.status(), Status::NotAcceptable);
        let status = String::from("Tickets length should be in range 1..10000");
        assert_eq!(response.into_string().unwrap(), status);

        let tickets = generate_tickets_json_for_insert(10001);
        let response = client.post("/api/v1/batch_insert").body(tickets).dispatch();
        assert_eq!(response.status(), Status::NotAcceptable);
        assert_eq!(response.into_string().unwrap(), status);
        clean_db();
    }

    #[test]
    fn should_not_insert_incorrect_type() {
        clean_db();
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let ticket = FakeTicket {
            not_tickets: vec![],
        };

        let tickets = json::serde_json::to_string(&ticket).unwrap();
        let response = client.post("/api/v1/batch_insert").body(tickets).dispatch();
        assert_eq!(response.status(), Status::UnprocessableEntity);
        clean_db();
    }

    #[test]
    fn should_update_ticket_info_on_same_id() {
        clean_db();
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let id1 = generate_id();
        let tickets: Tickets = Tickets {
            tickets: vec![Ticket {
                id: id1.clone(),
                departure_code: "LED".to_string(),
                arrival_code: "DME".to_string(),
                departure_time: 1636221467, // 2021-11-06
                arrival_time: 1636297066,
                price: 1500,
            }],
        };

        let body = json::serde_json::to_string(&tickets).unwrap();
        let response = client.post("/api/v1/batch_insert").body(body).dispatch();
        assert_eq!(response.status(), Status::Ok);

        let tickets: Tickets = Tickets {
            tickets: vec![Ticket {
                id: id1.clone(),
                departure_code: "LED".to_string(),
                arrival_code: "DME".to_string(),
                departure_time: 1637072073, // 2021-11-16
                arrival_time: 1637082873,
                price: 100,
            }],
        };

        let body = json::serde_json::to_string(&tickets).unwrap();
        let response = client.post("/api/v1/batch_insert").body(body).dispatch();
        assert_eq!(response.status(), Status::Ok);

        let search_data = SearchData {
            departure_code: "LED".to_string(),
            arrival_code: "DME".to_string(),
            departure_date: "2021-11-16".to_string(),
            limit: 100,
        };
        let body = json::serde_json::to_string(&search_data).unwrap();
        let response = client.post("/api/v1/search").body(&body).dispatch();
        assert_eq!(response.status(), Status::Ok);
        let status = SearchResult {
            solutions: vec![SearchSolution {
                ticket_ids: vec![id1],
                price: 100,
            }],
        };
        assert_eq!(
            response.into_string().unwrap(),
            json::serde_json::to_string(&status).unwrap()
        );
        clean_db();
    }

    #[test]
    fn should_correctly_search_tickets_and_sort_by_price() {
        clean_db();
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let id1 = generate_id();
        let id2 = generate_id();
        let id3 = generate_id();
        let id4 = generate_id();
        let tickets: Tickets = Tickets {
            tickets: vec![
                Ticket {
                    id: id1.clone(),
                    departure_code: "LED".to_string(),
                    arrival_code: "DME".to_string(),
                    departure_time: 1636221467,
                    arrival_time: 1636297066,
                    price: 1500,
                },
                Ticket {
                    id: id2.clone(),
                    departure_code: "DME".to_string(),
                    arrival_code: "JFK".to_string(),
                    departure_time: 1636315066,
                    arrival_time: 1636401466,
                    price: 2000,
                },
                Ticket {
                    id: id3.clone(),
                    departure_code: "LED".to_string(),
                    arrival_code: "JFK".to_string(),
                    departure_time: 1636221467,
                    arrival_time: 1636401466,
                    price: 10000,
                },
                Ticket {
                    id: id4.clone(),
                    departure_code: "LED".to_string(),
                    arrival_code: "JFK".to_string(),
                    departure_time: 1636221467,
                    arrival_time: 1636401469,
                    price: 10,
                },
            ],
        };
        let body = json::serde_json::to_string(&tickets).unwrap();
        let response = client.post("/api/v1/batch_insert").body(body).dispatch();
        assert_eq!(response.status(), Status::Ok);

        let search_data = SearchData {
            departure_code: "LED".to_string(),
            arrival_code: "JFK".to_string(),
            departure_date: "2021-11-06".to_string(),
            limit: 1000,
        };
        let body = json::serde_json::to_string(&search_data).unwrap();
        let response = client.post("/api/v1/search").body(&body).dispatch();
        assert_eq!(response.status(), Status::Ok);
        let status = SearchResult {
            solutions: vec![
                SearchSolution {
                    ticket_ids: vec![id4],
                    price: 10,
                },
                SearchSolution {
                    ticket_ids: vec![id1, id2],
                    price: 3500,
                },
                SearchSolution {
                    ticket_ids: vec![id3],
                    price: 10000,
                },
            ],
        };
        assert_eq!(
            response.into_string().unwrap(),
            json::serde_json::to_string(&status).unwrap()
        );
        clean_db();
    }

    #[test]
    fn should_search_respecting_limit_value() {
        clean_db();
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let id1 = generate_id();
        let id2 = generate_id();
        let id3 = generate_id();
        let tickets: Tickets = Tickets {
            tickets: vec![
                Ticket {
                    id: id1.clone(),
                    departure_code: "LED".to_string(),
                    arrival_code: "DME".to_string(),
                    departure_time: 1636221467,
                    arrival_time: 1636297066,
                    price: 1500,
                },
                Ticket {
                    id: id2.clone(),
                    departure_code: "LED".to_string(),
                    arrival_code: "DME".to_string(),
                    departure_time: 1636221467,
                    arrival_time: 1636297066,
                    price: 1501,
                },
                Ticket {
                    id: id3.clone(),
                    departure_code: "LED".to_string(),
                    arrival_code: "DME".to_string(),
                    departure_time: 1636221467,
                    arrival_time: 1636297066,
                    price: 1502,
                },
            ],
        };

        let body = json::serde_json::to_string(&tickets).unwrap();
        let response = client.post("/api/v1/batch_insert").body(body).dispatch();
        assert_eq!(response.status(), Status::Ok);

        let search_data = SearchData {
            departure_code: "LED".to_string(),
            arrival_code: "DME".to_string(),
            departure_date: "2021-11-06".to_string(),
            limit: 1,
        };
        let body = json::serde_json::to_string(&search_data).unwrap();
        let response = client.post("/api/v1/search").body(&body).dispatch();
        assert_eq!(response.status(), Status::Ok);
        let status = SearchResult {
            solutions: vec![SearchSolution {
                ticket_ids: vec![id1.clone()],
                price: 1500,
            }],
        };
        assert_eq!(
            response.into_string().unwrap(),
            json::serde_json::to_string(&status).unwrap()
        );

        let search_data = SearchData {
            departure_code: "LED".to_string(),
            arrival_code: "DME".to_string(),
            departure_date: "2021-11-06".to_string(),
            limit: 3,
        };
        let body = json::serde_json::to_string(&search_data).unwrap();
        let response = client.post("/api/v1/search").body(&body).dispatch();
        assert_eq!(response.status(), Status::Ok);
        let status = SearchResult {
            solutions: vec![
                SearchSolution {
                    ticket_ids: vec![id1],
                    price: 1500,
                },
                SearchSolution {
                    ticket_ids: vec![id2],
                    price: 1501,
                },
                SearchSolution {
                    ticket_ids: vec![id3],
                    price: 1502,
                },
            ],
        };

        assert_eq!(
            response.into_string().unwrap(),
            json::serde_json::to_string(&status).unwrap()
        );
        clean_db();
    }

    #[test]
    fn should_search_respecting_departure_date() {
        clean_db();
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let id1 = generate_id();
        let id2 = generate_id();
        let id3 = generate_id();
        let tickets: Tickets = Tickets {
            tickets: vec![
                Ticket {
                    id: id1.clone(),
                    departure_code: "LED".to_string(),
                    arrival_code: "DME".to_string(),
                    departure_time: 1636221467, // 2021-11-06
                    arrival_time: 1636297066,
                    price: 1500,
                },
                Ticket {
                    id: id2.clone(),
                    departure_code: "LED".to_string(),
                    arrival_code: "DME".to_string(),
                    departure_time: 1636992873, // 2021-11-15
                    arrival_time: 1637072074,
                    price: 1,
                },
                Ticket {
                    id: id3.clone(),
                    departure_code: "LED".to_string(),
                    arrival_code: "DME".to_string(),
                    departure_time: 1636985673, // 2021-11-15
                    arrival_time: 1637072073,
                    price: 1000000,
                },
            ],
        };

        let body = json::serde_json::to_string(&tickets).unwrap();
        let response = client.post("/api/v1/batch_insert").body(body).dispatch();
        assert_eq!(response.status(), Status::Ok);

        let search_data = SearchData {
            departure_code: "LED".to_string(),
            arrival_code: "DME".to_string(),
            departure_date: "2021-11-15".to_string(),
            limit: 100,
        };
        let body = json::serde_json::to_string(&search_data).unwrap();
        let response = client.post("/api/v1/search").body(&body).dispatch();
        assert_eq!(response.status(), Status::Ok);
        let status = SearchResult {
            solutions: vec![
                SearchSolution {
                    ticket_ids: vec![id2],
                    price: 1,
                },
                SearchSolution {
                    ticket_ids: vec![id3],
                    price: 1000000,
                },
            ],
        };
        assert_eq!(
            response.into_string().unwrap(),
            json::serde_json::to_string(&status).unwrap()
        );
        clean_db();
    }

    #[test]
    fn should_not_find_tickets_from_and_to_the_same_airport() {
        clean_db();
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let id1 = generate_id();
        let id2 = generate_id();
        let tickets: Tickets = Tickets {
            tickets: vec![
                Ticket {
                    // Let's assume this ticket somehow ended up in the db
                    id: id1,
                    departure_code: "LED".to_string(),
                    arrival_code: "LED".to_string(),
                    departure_time: 1636221467,
                    arrival_time: 1636297066,
                    price: 1500,
                },
                Ticket {
                    id: id2,
                    departure_code: "DME".to_string(),
                    arrival_code: "DME".to_string(),
                    departure_time: 1636992873,
                    arrival_time: 1637072074,
                    price: 100,
                },
            ],
        };

        let body = json::serde_json::to_string(&tickets).unwrap();
        let response = client.post("/api/v1/batch_insert").body(body).dispatch();
        assert_eq!(response.status(), Status::Ok);

        let search_data = SearchData {
            departure_code: "LED".to_string(),
            arrival_code: "DME".to_string(),
            departure_date: "2021-11-15".to_string(),
            limit: 100,
        };
        let body = json::serde_json::to_string(&search_data).unwrap();
        let response = client.post("/api/v1/search").body(&body).dispatch();
        assert_eq!(response.status(), Status::Ok);
        let status = SearchResult { solutions: vec![] };
        assert_eq!(
            response.into_string().unwrap(),
            json::serde_json::to_string(&status).unwrap()
        );

        let search_data = SearchData {
            departure_code: "DME".to_string(),
            arrival_code: "DME".to_string(),
            departure_date: "2021-11-15".to_string(),
            limit: 100,
        };
        let body = json::serde_json::to_string(&search_data).unwrap();
        let response = client.post("/api/v1/search").body(&body).dispatch();
        assert_eq!(response.status(), Status::Ok);
        let status = SearchResult { solutions: vec![] };
        assert_eq!(
            response.into_string().unwrap(),
            json::serde_json::to_string(&status).unwrap()
        );
        clean_db();
    }

    #[test]
    fn should_not_find_transfers_if_time_between_flights_is_too_low() {
        clean_db();
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let id1 = generate_id();
        let id2 = generate_id();
        let tickets: Tickets = Tickets {
            tickets: vec![
                Ticket {
                    id: id1,
                    departure_code: "LED".to_string(),
                    arrival_code: "DME".to_string(),
                    departure_time: 1636221467, // 2021-11-06
                    arrival_time: 1636297066,
                    price: 1500,
                },
                Ticket {
                    id: id2,
                    departure_code: "DME".to_string(),
                    arrival_code: "JFK".to_string(),
                    departure_time: 1636305840, // 2 hours 30 minutes after arrival
                    arrival_time: 1636305880,
                    price: 10000,
                },
            ],
        };

        let body = json::serde_json::to_string(&tickets).unwrap();
        let response = client.post("/api/v1/batch_insert").body(body).dispatch();
        assert_eq!(response.status(), Status::Ok);

        let search_data = SearchData {
            departure_code: "LED".to_string(),
            arrival_code: "JFK".to_string(),
            departure_date: "2021-11-06".to_string(),
            limit: 100,
        };
        let body = json::serde_json::to_string(&search_data).unwrap();
        let response = client.post("/api/v1/search").body(&body).dispatch();
        assert_eq!(response.status(), Status::Ok);
        let status = SearchResult { solutions: vec![] };
        assert_eq!(
            response.into_string().unwrap(),
            json::serde_json::to_string(&status).unwrap()
        );

        clean_db();
    }

    #[test]
    fn should_not_find_transfers_if_time_between_flights_is_too_high() {
        clean_db();
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let id1 = generate_id();
        let id2 = generate_id();
        let tickets: Tickets = Tickets {
            tickets: vec![
                Ticket {
                    id: id1,
                    departure_code: "LED".to_string(),
                    arrival_code: "DME".to_string(),
                    departure_time: 1636221467, // 2021-11-06
                    arrival_time: 1636297066,
                    price: 1500,
                },
                Ticket {
                    id: id2,
                    departure_code: "DME".to_string(),
                    arrival_code: "JFK".to_string(),
                    departure_time: 1636383540, // 24 hours 2 minutes after arrival
                    arrival_time: 1636305880,
                    price: 10000,
                },
            ],
        };

        let body = json::serde_json::to_string(&tickets).unwrap();
        let response = client.post("/api/v1/batch_insert").body(body).dispatch();
        assert_eq!(response.status(), Status::Ok);

        let search_data = SearchData {
            departure_code: "LED".to_string(),
            arrival_code: "JFK".to_string(),
            departure_date: "2021-11-06".to_string(),
            limit: 100,
        };
        let body = json::serde_json::to_string(&search_data).unwrap();
        let response = client.post("/api/v1/search").body(&body).dispatch();
        assert_eq!(response.status(), Status::Ok);
        let status = SearchResult { solutions: vec![] };
        assert_eq!(
            response.into_string().unwrap(),
            json::serde_json::to_string(&status).unwrap()
        );

        clean_db();
    }

    fn generate_random_ticket() -> Ticket {
        let mut rng = rand::thread_rng();
        let id = generate_id();
        let departure_code = generate_rnd_iata();
        let arrival_code = generate_rnd_iata();
        let departure_time = rng.gen_range(1631783374..1636783374);
        let arrival_time = rng.gen_range(departure_time..departure_time + 500000);
        let price = rng.gen_range(2000..25000);

        Ticket {
            id,
            departure_code,
            arrival_code,
            departure_time,
            arrival_time,
            price,
        }
    }

    fn generate_rnd_iata() -> String {
        let mut rng = rand::thread_rng();
        let iata_list = vec![
            "LED", "DME", "BRO", "AVG", "AAJ", "AMS", "SND", "BFF", "BER", "TWG",
        ];

        iata_list.choose(&mut rng).unwrap().to_string()
    }

    fn generate_id() -> String {
        let mut rng = rand::thread_rng();
        iter::repeat(())
            .map(|()| rng.sample(Alphanumeric))
            .map(char::from)
            .take(32)
            .collect()
    }

    fn generate_tickets_json_for_insert(quantity: usize) -> String {
        let mut tickets: Tickets = Tickets { tickets: vec![] };

        for _ in 0..quantity {
            let t = generate_random_ticket();
            tickets.tickets.push(t);
        }

        json::serde_json::to_string(&tickets).unwrap()
    }

    fn clean_db() {
        #[derive(Deserialize)]
        #[serde(crate = "rocket::serde")]
        struct Config {
            databases: Db,
        }

        #[derive(Deserialize)]
        #[serde(crate = "rocket::serde")]
        struct Db {
            mydb: Url,
        }

        #[derive(Deserialize)]
        #[serde(crate = "rocket::serde")]
        struct Url {
            url: String,
        }

        let url: Config = rocket().figment().extract().unwrap();
        let mut client =
            postgres::Client::connect(&url.databases.mydb.url, postgres::NoTls).unwrap();

        client.execute("DELETE FROM tickets;", &[]).unwrap();
    }
}
