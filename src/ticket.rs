use rocket::response::{self, Responder};
use rocket::serde::json;
use rocket::serde::{Deserialize, Serialize};
use rocket::{http::Status, serde::json::Json};
use rocket::{Request, Response};
use std::io::Cursor;

use crate::DbConnection;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct Ticket {
    pub id: String,
    pub departure_code: String,
    pub arrival_code: String,
    pub departure_time: i64,
    pub arrival_time: i64,
    pub price: i64,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct Tickets {
    pub tickets: Vec<Ticket>,
}

#[derive(Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct StatusResponse {
    pub status: String,
}

#[catch(406)]
pub fn not_acceptable() -> String {
    String::from("Tickets length should be in range 1..10000")
}

impl<'r> Responder<'r, 'static> for StatusResponse {
    fn respond_to(self, _: &'r Request<'_>) -> response::Result<'static> {
        let body = json::serde_json::to_string(&self).unwrap();
        Response::build()
            .sized_body(body.len(), Cursor::new(body))
            .ok()
    }
}

#[post("/batch_insert", data = "<tickets>")]
pub async fn batch_insert(
    tickets: Json<Tickets>,
    conn: DbConnection,
) -> Result<StatusResponse, Status> {
    let len = tickets.tickets.len();

    if len == 0 || len > 10000 {
        return Err(Status::NotAcceptable);
    }

    let mut all_values_to_insert = String::from("");
    let mut iter = tickets.tickets.iter().peekable();
    while let Some(ticket) = iter.next() {
        let value_row = format!(
            "('{}', '{}', '{}', {}, {}, {})",
            &ticket.id,
            &ticket.departure_code,
            &ticket.arrival_code,
            &ticket.departure_time,
            &ticket.arrival_time,
            &ticket.price,
        );
        let mut last_symbol: &str = ",";
        // When last ticket is being iterated on
        if iter.peek().is_none() {
            last_symbol = ""
        }
        all_values_to_insert.push_str(&value_row);
        all_values_to_insert.push_str(&last_symbol);
    }

    let result_query = format!(
        "
        INSERT INTO tickets
            (id, departure_code, arrival_code, departure_time, arrival_time, price)
        VALUES
                {}
        ON CONFLICT (id)
        DO UPDATE SET
            departure_time = EXCLUDED.departure_time,
            arrival_time = EXCLUDED.arrival_time,
            price = EXCLUDED.price;",
        all_values_to_insert
    );


    let insert_result = conn.run(move |conn| conn.execute(&result_query, &[])).await;

    match insert_result {
        Ok(_) => Ok(StatusResponse {
            status: String::from("success"),
        }),
        Err(err) => {
            eprintln!("{}", err);
            return Err(Status::InternalServerError);
        }
    }
}
